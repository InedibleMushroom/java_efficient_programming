package lambda.file;

import java.io.*;

/**
 * <h1>文件服务类</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-21 21:46
 */
public class FileService {

    /**
     * <h2>通过url获取本地文件，调用函数式接口处理</h2>
     * @param url {@link String} 文件地址url
     * @param fileConsumer {@link FileConsumer} 文件处理函数式接口
     * @return {@link }
     * @auther InedibleMushroom
     * @date 2019/9/21
     */
    public void fileHandler(String url, FileConsumer fileConsumer) throws IOException {

        // 创建文件读取流
        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(url)));

        // 定义行变量和内容sb
        String line;
        StringBuilder stringBuilder = new StringBuilder();

        while ((line = bufferedReader.readLine()) != null){
            stringBuilder.append(line + "/n");
        }

        // 调用函数式接口，将文件传递给lambda表达式
        fileConsumer.fileHandler(stringBuilder.toString());
    }
}
