package lambda.file;

/**
 * <h1>文件处理函数式接口</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-21 21:48
 */
@FunctionalInterface
public interface FileConsumer {

    /**
     * <h2>函数式接口抽象方法</h2>
     * @param file {@link String} 文件内容字符串
     * @return {@link }
     * @auther InedibleMushroom
     * @date 2019/9/21
     */
    void fileHandler(String file);
}
