package lambda.cart;

/**
 * <h1>根据总价判断</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-21 20:30
 */
public class SkuTotalPricesPredicate implements SkuPredicate {
    public boolean test(Sku sku) {
        return sku.getTotalPrice() > 2000;
    }
}
