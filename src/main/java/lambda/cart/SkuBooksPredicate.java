package lambda.cart;

/**
 * <h1>对book进行判断</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-21 20:28
 */
public class SkuBooksPredicate implements SkuPredicate {

    public boolean test(Sku sku) {
        return SkuCategoryEnum.BOOKS.equals(sku.getSkuCategory());
    }
}
