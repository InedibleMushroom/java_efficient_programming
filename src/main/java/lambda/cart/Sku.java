package lambda.cart;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <h1>下单商品信息对象</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-21 18:56
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Sku {

    /** 编号 */
    private Integer skuId;


    /** 商品名称 */
    private String name;


    /** 单价 */
    private Double skuPrice;


    /** 购买个数 */
    private Integer totalNum;


    /** 总价 */
    private Double totalPrice;


    /** 商品类型 */
    private Enum skuCategory;
}
