package lambda.cart;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * <h1>商品类型枚举</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-21 19:03
 */
@AllArgsConstructor
@NoArgsConstructor
public enum SkuCategoryEnum {

    CLOTHING(10, "服装类"),
    ELECTRONICE(20, "数码类"),
    SPORTS(30, "运动类"),
    BOOKS(40, "图书类");

    /** 商品类型编号 */
    private Integer code;

    /** 商品类型名称 */
    private String name;
}
