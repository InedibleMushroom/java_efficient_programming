package lambda.cart;

/**
 * <h1>Sku选择谓词接口</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-21 20:15
 */
public interface SkuPredicate {

    /**
     * <h2>选择判断标准</h2>
     * @param sku {@link Sku}
     * @return {@link Boolean}
     * @auther InedibleMushroom
     * @date 2019-09-21
     */
    boolean test(Sku sku);
}
