package lambda.cart;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>购物车服务类</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-21 19:02
 */
public class CartService {

    /** 加入到购物车中的商品信息 */
    private static List<Sku> cartSkuList =
            new ArrayList<Sku>(){
                {
                    add(new Sku(654032, "无人机",
                            4999.00, 1,
                            4999.00, SkuCategoryEnum.ELECTRONICE));

                    add(new Sku(642934, "VR一体机",
                            2299.00, 1,
                            2299.00, SkuCategoryEnum.ELECTRONICE));

                    add(new Sku(645321, "纯色衬衫",
                            409.00, 3,
                            1227.00, SkuCategoryEnum.CLOTHING));

                    add(new Sku(654327, "牛仔裤",
                            528.00, 1,
                            528.00, SkuCategoryEnum.CLOTHING));

                    add(new Sku(675489, "跑步机",
                            2699.00, 1,
                            2699.00, SkuCategoryEnum.SPORTS));

                    add(new Sku(644564, "Java编程思想",
                            79.80, 1,
                            79.80, SkuCategoryEnum.BOOKS));

                    add(new Sku(678678, "Java核心技术",
                            149.00, 1,
                            149.00, SkuCategoryEnum.BOOKS));

                    add(new Sku(697894, "算法",
                            78.20, 1,
                            78.20, SkuCategoryEnum.BOOKS));

                    add(new Sku(696968, "TensorFlow进阶指南",
                            85.10, 1,
                            85.10, SkuCategoryEnum.BOOKS));
                }
            };


    /**
     * <h2>返回商品信息列表</h2>
     * Version 1.0.0
     * @param:  {@link }
     * @return: cartSkuList {@link List}
     * @auther: InedibleMushroom
     * @date: 2019-09-21
     */
    public static List<Sku> getCartSkuList() {
        return cartSkuList;
    }


    /**
     * <h2>找出购物车中所有电子产品</h2>
     * @param: cartSkuList {@link List}
     * @return:  {@link List}
     * @auther: InedibleMushroom
     * @date: 2019-09-21
     */
    public static List<Sku> filterElectronicsSkus(List<Sku> cartSkuList){

        List<Sku> result = new ArrayList<Sku>();
        for (Sku sku : cartSkuList){
            if (SkuCategoryEnum.ELECTRONICE.equals(sku.getSkuCategory())){
                result.add(sku);
            }
        }

        return result;
    }


    /**
     * <h2>根据商品类型参数，找出同种商品类型列表</h2>
     * 单一纬度条件参数化
     * version 2.0.0
     * @param cartSkuList {@link List}
     * @param category {@link SkuCategoryEnum}
     * @return: {@link List}
     * @auther: InedibleMushroom
     * @date: 2019-09-21
     */
    public static List<Sku> filterSkusByCategory(List<Sku> cartSkuList, SkuCategoryEnum category){
        List<Sku> result = new ArrayList<Sku>();
        for (Sku sku : cartSkuList){
            if (category.equals(sku.getSkuCategory())){
                result.add(sku);
            }
        }

        return result;
    }


    /**
     * <h2>支持通过商品类型或者总价来过滤</h2>
     * 多维度条件参数化
     * version 3.0.0
     * @param skus {@link List}
     * @param category {@link SkuCategoryEnum}
     * @param bool {@link Boolean}
     * @return {@link List}
     * @auther InedibleMushroom
     * @date 2019-09-21
     */
    public static List<Sku> filterSkus(List<Sku> skus, SkuCategoryEnum category, Double totalPrice, Boolean bool){
        List<Sku> result = new ArrayList<Sku>();
        for (Sku sku : cartSkuList){
            if ((bool && category.equals(sku.getSkuCategory())) || (!bool && sku.getTotalNum() > totalPrice)){
                result.add(sku);
            }
        }

        return result;
    }


    /**
     * <h2>根据不同的sku判断标准，对sku列表进行过滤</h2>
     * version 4.0.0
     * @param cartSkuList {@link List}
     * @param predicate {@link SkuPredicate} 不同的sku判断策略
     * @return {@link List}
     * @auther InedibleMushroom
     * @date 2019-09-21
     */
    public static List<Sku> filterPredicate(List<Sku> cartSkuList, SkuPredicate predicate){
        List<Sku> result = new ArrayList<Sku>();
        for (Sku sku : cartSkuList){
            // 根据不同的sku判断策略，对sku进行判断
            if (predicate.test(sku)){
                result.add(sku);
            }
        }

        return result;
    }

}
