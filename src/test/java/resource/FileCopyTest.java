package resource;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * <h1>Jdk7之前的文件拷贝功能</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-22 12:23
 */
public class FileCopyTest {

    /**
     * 1. 创建输入、输出流
     * 2. 执行文件拷贝，读取文件内容，写入到另一个文件中
     * 3. **关闭文件资源**
     */
    @Test
    public void test(){
        // 定义输入输出路径
        String originUrl = "lib/FileCopyTest.java";
        String targetUrl = "targetTest/target.txt";

        // 声明文件输入输出流
        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;

        try {
            // 实例化文件流对象
            inputStream = new FileInputStream(originUrl);
            outputStream = new FileOutputStream(targetUrl);

            // 读取的字节信息
            int content;

            // 迭代，读取信息
            while ((content = inputStream.read()) != -1){
                outputStream.write(content);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭流资源
            if (outputStream != null){
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }



}
