package resource;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * <h1>使用TWR方式资源关闭，Jdk1.7之后</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-22 12:38
 */
public class NewFileCopyTest {
    /**
     * 1. 创建输入、输出流
     * 2. 执行文件拷贝，读取文件内容，写入到另一个文件中
     * 3. **关闭文件资源**
     */
    @Test
    public void test() {
        // 定义输入输出路径
        String originUrl = "lib/FileCopyTest.java";
        String targetUrl = "targetTest/target.txt";

        // 初始化输入/输出对象
        // try-with-resource 方法
        // 在try()中声明资源
        // 在try{}中执行
        // 不用在finally中显示的关闭流
        try(
                FileInputStream inputStream = new FileInputStream(originUrl);
                FileOutputStream outputStream = new FileOutputStream(targetUrl);
                ){

            // 读取的字节信息
            int content;

            // 迭代，读取信息
            while ((content = inputStream.read()) != -1){
                outputStream.write(content);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
