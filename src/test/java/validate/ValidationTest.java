package validate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.executable.ExecutableValidator;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;

/**
 * <h1>验证测试类<h1>
 *
 * @author: InedibleMushroom
 * @create: 2019-11-06 16:48
 **/
public class ValidationTest {

    /** 验证器对象 */
    private Validator validator;

    /** 待验证对象 */
    private UserInfo userInfo;

    /** 验证结果集合 */
    private Set<ConstraintViolation<UserInfo>> set;

    /** 验证结果集合 */
    private Set<ConstraintViolation<UserInfoService>> otherSet;


    @Before
    public void init(){
        // 初始化验证器
        validator = Validation.buildDefaultValidatorFactory().getValidator();

        userInfo = new UserInfo();
        //userInfo.setUserId("88888");
        userInfo.setUserName("王腾");
        userInfo.setPassword("123456");
        //userInfo.setEmail("2970565888@qq.com");
        userInfo.setPhone("13714022222");
        userInfo.setAge(22);

        Calendar instance = Calendar.getInstance();
        instance.set(1997, 8,17);
        userInfo.setBirthday(instance.getTime());

        UserInfo friend = new UserInfo();
        friend.setUserId("777777777");
        friend.setUserName("InedibleMushroom");
        friend.setPassword("222222");
        userInfo.setFriends(new ArrayList(){{add(friend);}});
    }


    @After
    public void print(){
        otherSet.forEach(item -> {
            // 输出验证信息
            System.out.println(item.getMessage());
        });
    }

    @Test
    public void nullValidation(){
        // 使用验证器对对象进行验证
         set = validator.validate(userInfo);
    }

    /** 级联验证 */
    @Test
    public void graphValidation(){
        set = validator.validate(userInfo);
    }

    /** 分组验证 选择一个组后，未分组的也不会进行验证*/
    @Test
    public void groupValidation(){
        set = validator.validate(userInfo, UserInfo.RegisterGroup.class, UserInfo.LoginGroup.class);
    }

    /** 组序列 当验证出一个不通过时，就会跳出验证 */
    @Test
    public void groupSequenceValidation(){
        set = validator.validate(userInfo, UserInfo.Group.class);
    }

    /** 参数校验 */
    @Test
    public void paramValidation() throws NoSuchMethodException {
        // 获取校验执行器
        ExecutableValidator executableValidator = validator.forExecutables();

        UserInfoService userInfoService = new UserInfoService();
        // 方法
        Method method = userInfoService.getClass().getMethod("setUserInfo", UserInfo.class);
        // 方法输入参数参数
        Object[] paramObjects = new Object[]{new UserInfo()};

        // 对方法的输入参数进行校验
        otherSet = executableValidator.validateParameters(userInfoService, method, paramObjects);
    }

    /** 返回值校验 */
    @Test
    public void returnValidation() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // 获取校验执行器
        ExecutableValidator executableValidator = validator.forExecutables();

        UserInfoService userInfoService = new UserInfoService();
        // 方法
        Method method = userInfoService.getClass().getMethod("getUserInfo");

        // 调用方法得到返回值
        Object invoke = method.invoke(userInfoService);

        // 对方法的返回值进行校验
        otherSet = executableValidator.validateReturnValue(userInfoService, method, invoke);
    }

    /** 构造函数校验 */
    @Test
    public void constructorValidation() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // 获取校验执行器
        ExecutableValidator executableValidator = validator.forExecutables();

        UserInfoService userInfoService = new UserInfoService();
        // 获取构造函数
        Constructor constructor = userInfoService.getClass().getConstructor(UserInfo.class);

        // 方法输入参数参数
        Object[] paramObjects = new Object[]{new UserInfo()};

        // 对方法的构造函数进行校验
        otherSet = executableValidator.validateConstructorParameters(constructor, paramObjects);
    }
}
