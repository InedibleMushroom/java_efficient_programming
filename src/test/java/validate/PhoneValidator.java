package validate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <h1>约束注解关联的验证器<h1>
 *
 * @author: InedibleMushroom
 * @create: 2019-11-06 18:29
 **/
public class PhoneValidator implements ConstraintValidator<Phone, String> {

    /**
     * 自定义校验逻辑方法
     * @param s
     * @param constraintValidatorContext
     * @return
     */
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {

        // 手机号176开头后面随便
        String check = "158\\d{8}";
        Pattern compile = Pattern.compile(check);

        String phone = Optional.ofNullable(s).orElse(" ");
        Matcher matcher = compile.matcher(phone);

        return matcher.matches();
    }
}
