package validate;

import javax.validation.Valid;

/**
 * <h1>用户信息服务类<h1>
 *
 * @author: InedibleMushroom
 * @create: 2019-11-06 17:47
 **/
public class UserInfoService {

    /**
     * 对输入参数进行校验
     * @param userInfo
     */
    public void setUserInfo(@Valid UserInfo userInfo){
    }

    /**
     * 对输出参数进行校验
     * @return
     */
    public @Valid UserInfo getUserInfo(){
        return new UserInfo();
    }

    /**
     * 默认构造函数
     */
    public UserInfoService(){
    }

    /**
     * 接收参数的构造函数
     * @param userInfo
     */
    public UserInfoService(@Valid UserInfo userInfo){

    }
}
