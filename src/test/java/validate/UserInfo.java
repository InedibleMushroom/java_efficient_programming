package validate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.GroupSequence;
import javax.validation.Valid;
import javax.validation.constraints.*;
import javax.validation.groups.Default;
import java.util.Date;
import java.util.List;

/**
 * <h1>待验证对象<h1>
 * 用户信息类
 * @author: InedibleMushroom
 * @create: 2019-11-06 16:15
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo {

    /** 登录场景 */
    public interface LoginGroup{}

    /** 注册场景 */
    public interface RegisterGroup{}

    /** 组排序场景*/
    @GroupSequence({
            LoginGroup.class,
            RegisterGroup.class,
            Default.class
    })
    public interface Group{}

    /** 分组验证 */
    @NotNull(message = "用户ID不能为空", groups = LoginGroup.class)
    private String userId;

    @NotEmpty(message = "用户名不能为空")
    private String userName;

    @NotBlank(message = "用户名密码不能为空")
    @Length(min = 6, max = 20, message = "密码不能小于6位或大于20位")
    private String password;

    @NotNull(message = "邮箱不能为空", groups = RegisterGroup.class)
    @Email(message = "邮箱格式不正确")
    private String email;

    @Phone(message = "手机号不是176开头")
    private String phone;

    @Min(value = 18, message = "年龄不能小于18")
    @Max(value = 60, message = "年龄不能大于60")
    private Integer age;

    @Past(message = "日期不能是未来时间点")
    private Date birthday;

    /** 级联验证 @Valid */
    @Size(min = 1, message = "不能少于1个好友")
    private List<@Valid UserInfo> friends;

}
