package guava;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * <h1>不可变集合使用</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-22 13:35
 */
public class ImmutableTest {

    public static void testList(List<Integer> list){
        list.remove(0);
    }

    @Test
    public void test() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);

        testList(list);

        // [2, 3]
        System.out.println(list);

        // 修改为不可变集合
        List<Integer> newList = Collections.unmodifiableList(list);
        // 抛出异常 UnsupportedOperationException
        testList(newList);
        System.out.println(newList);
    }

    /** Guava提供的创建不可变集合的方法 */
    @Test
    public void testGuava(){
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);

        // copyOf方法：ImmutableSet.copySet()
        // 通过已存在集合创建
        ImmutableSet immutableSet = ImmutableSet.copyOf(list);

        // of方法：ImmutableSet.of("a", "b", "c")
        // 通过值直接创建
        ImmutableSet immutableSet1 = ImmutableSet.of("a", "b", "c");

        // Builder工具：Immutable.builder().build()
        ImmutableSet immutableSet2 = ImmutableSet.builder()
                .add(1)
                .addAll(Sets.newHashSet(3,4))
                .add(5)
                .build();

        immutableSet.forEach(System.out::println);
        immutableSet1.forEach(System.out::println);
        immutableSet2.forEach(System.out::println);
    }
}
