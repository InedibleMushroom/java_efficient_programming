package guava;

import lambda.cart.CartService;
import lambda.cart.Sku;
import org.junit.Test;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * <h1>Optional使用 Java8</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-22 13:08
 */
public class OptionalTest {
    @Test
    public void test(){

        // 创建一个空的Optional对象
        Optional.empty();

        // 创建一个非null值的Optional对象
        Optional.of("InedibleMushroom");

        // 可以接收任何值
        Optional optional = Optional.ofNullable(null);


        //判断是否引用缺失,建议不直接使用
        System.out.println(optional.isPresent());

        //如果存在，执行方法，接收一个Consumer函数式接口
        optional.ifPresent(System.out::println);

        // 当optional引用缺失情况下，默认赋的值
        optional.orElse("引用缺失");
        optional.orElseGet(() -> {
            return "自定义缺失值";
        });
    }

    /** Optional在Stream中使用 */
    @Test
    public void testOptionalInStream(){
        List<Sku> list = CartService.getCartSkuList();

        Optional.ofNullable(list)
                .map(List::stream)
                .orElseGet(Stream::empty)
                .forEach(System.out::println);
    }
}
