package guava;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.junit.Test;

import java.util.List;
import java.util.Set;

/**
 * <h1>使用工具类操作set/list</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-22 14:28
 */
public class SetsTest {

    /**
     * Sets工具类的常用方法
     * 并集 / 交集 / 差集 / 分解集合中的所有子集 / 求两个集合的笛卡尔积
     *
     * Lists工具类的常用方式
     * 反转 / 拆分
     */

    private static final Set set1 =
            Sets.newHashSet(1, 2, 4);

    private static final Set set2 =
            Sets.newHashSet(4, 5);

    /** 测试Sets */
    @Test
    public void testSets(){
        // 并集
        Set<Integer> setUnion = Sets.union(set1, set2);
        System.out.println(setUnion);

        // 交集
        Set<Integer> setIntersection = Sets.intersection(set1, set2);
        System.out.println(setIntersection);

        // 差集：如果元素属于A而且不属于B
        Set<Integer> setDifference = Sets.difference(set1, set2);
        System.out.println(setDifference);

        // 相对差集：属于A而且不属于B 或者 属于B而且不属于A
        Set<Integer> setSymmetric = Sets.symmetricDifference(set1, set2);
        System.out.println(setSymmetric);

        // 拆分所有子集合
        Set<Set<Integer>> powerSet = Sets.powerSet(set1);
        System.out.println(JSON.toJSONString(powerSet));

        // 计算两个集合笛卡尔积
        Set<List<Integer>> product =
                Sets.cartesianProduct(set1, set2);
        System.out.println(JSON.toJSONString(product));
    }

    /** 测试lists使用 */
    @Test
    public void testList(){
        // 拆分
        List<Integer> list =
                Lists.newArrayList(1, 2, 3, 4, 5, 6, 7);
        List<List<Integer>> partition =
                Lists.partition(list, 3);
        System.out.println(JSON.toJSONString(partition));


        // 反转
        List<Integer> list1 = Lists.newLinkedList();
        list1.add(1);
        list1.add(2);
        list1.add(3);
        List<Integer> newList = Lists.reverse(list1);
        System.out.println(newList);
    }
}
