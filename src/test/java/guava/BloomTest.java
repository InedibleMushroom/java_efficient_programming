package guava;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.PrimitiveSink;
import org.junit.Test;

/**
 * <h1>布隆过滤器使用</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-22 15:25
 */
public class BloomTest {

    @Test
    public void test(){
        /**
         * 创建布隆过滤器对象
         */
        BloomFilter<Integer> bloomFilter = BloomFilter.create(
                // 将任意类型数据转化为Java基础数据类型，默认用jav.nio.ByteBuffer实现，最终转化为byte数组
                (Integer from, PrimitiveSink primitiveSink) -> primitiveSink.putInt(from),

                // 预计插入的元素总数
                1000L,

                // 期望误判率（0.0 ~ 1.0）
                0.5
        );

        /**
         * 向布隆过滤器中添加元素
         */
        for (int i = 0; i < 10000 ; i++){
            bloomFilter.put(i);
        }

        /**
         * 判断给定元素是否可能在布隆过滤器中
         */
        boolean might = bloomFilter.mightContain(999);
        System.out.println("是否存在？" + might);
    }

}
