package lambda.file;

import org.junit.Test;

import java.io.IOException;

/**
 * <h1>FileService 测试类</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-21 22:00
 */
public class FileServiceTest {

    @Test
    public void fileHandler() throws IOException {
        new FileService().fileHandler("/Users/wangteng/GitRepo/java_efficient_programming/EfficientPrograming/src/test/java/lambda/file/FileServiceTest.java", file -> System.out.println(file));
    }
}
