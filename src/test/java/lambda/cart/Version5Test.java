package lambda.cart;

import com.alibaba.fastjson.JSON;
import org.junit.Test;

import java.util.List;

/**
 * <h1>对version 4.0.0 的改进，使用匿名类</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-21 20:40
 */
public class Version5Test {
    @Test
    public void filterPredicate(){
        List<Sku> skus = CartService.getCartSkuList();

        List<Sku> result = CartService.filterPredicate(skus, new SkuPredicate() {
            public boolean test(Sku sku) {
                return sku.getTotalPrice() > 1000;
            }
        });

        System.out.println(JSON.toJSONString(result, true));
    }
}
