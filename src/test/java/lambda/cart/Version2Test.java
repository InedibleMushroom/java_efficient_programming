package lambda.cart;

import com.alibaba.fastjson.JSON;
import org.junit.Test;

import java.util.List;

/**
 * <h1>version2.0.0 版本测试</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-21 19:59
 */
public class Version2Test {

    @Test
    public void filterSkusByCategory() {
        List<Sku> cartSkuList = CartService.getCartSkuList();

        List<Sku> result = CartService.filterSkusByCategory(cartSkuList, SkuCategoryEnum.BOOKS);

        System.out.println(JSON.toJSONString(result,true));


    }
}
