package lambda.cart;

import com.alibaba.fastjson.JSON;
import org.junit.Test;

import java.util.List;

/**
 * <h1>version 1.0.0 版本测试</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-21 19:43
 */
public class Version1Test {

    @Test
    public void filterElectronicsSkus() {
        List<Sku> cartSkuList = CartService.getCartSkuList();

        List<Sku> result = CartService.filterElectronicsSkus(cartSkuList);

        System.out.println(JSON.toJSONString(result,true));


    }
}
