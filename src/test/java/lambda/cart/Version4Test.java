package lambda.cart;

import com.alibaba.fastjson.JSON;
import org.junit.Test;

import java.util.List;

/**
 * <h1>version 4.0.0 版本测试</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-21 20:33
 */
public class Version4Test {
    @Test
    public void filterPredicate(){
        List<Sku> skus = CartService.getCartSkuList();

        List<Sku> result = CartService.filterPredicate(skus, new SkuBooksPredicate());

        System.out.println(JSON.toJSONString(result, true));
    }
}
