package lambda.cart;

import com.alibaba.fastjson.JSON;
import org.junit.Test;

import java.util.List;

/**
 * <h1>改进 version5</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-21 20:43
 */
public class Version6Test {

    @Test
    public void filterPredicate(){
        List<Sku> skus = CartService.getCartSkuList();

        List<Sku> result = CartService.filterPredicate(skus, sku -> sku.getSkuPrice() > 1000);

        System.out.println(JSON.toJSONString(result, true));
    }
}
