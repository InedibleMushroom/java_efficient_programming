package lambda.cart;

import com.alibaba.fastjson.JSON;
import org.junit.Test;

import java.util.List;

/**
 * <h1>version 3.0.0 版本测试</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-21 20:09
 */
public class Version3Test {

    @Test
    public void filterSkus() {

        List<Sku> cartSkuList = CartService.getCartSkuList();

        List<Sku> result = CartService.filterSkus(cartSkuList, null, 2000.00, false);

        System.out.println(JSON.toJSONString(result,true));


    }
}
