package lambda.stream;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import lambda.cart.CartService;
import lambda.cart.Sku;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * <h1>常见的预定义收集器使用</h1>
 *
 * Collector<T, A, R>
 * @auther: InedibleMushroom
 * @create: 2019-09-22 11:27
 */
public class StreamCollector {

    List<Sku> skus;

    @Before
    public void init(){
        skus = CartService.getCartSkuList();
    }

    /** toList 集合收集器使用 */
    @Test
    public void testToList(){
        List<String> list = skus.stream()
                .map(sku -> sku.getName())
                .collect(Collectors.toList());

        list.forEach(System.out::println);
    }

    /** groupingBy 分组收集 */
    @Test
    public void testGroup(){

        // Map<分组条件，集合>
        Map<Object, List<Sku>> map = skus.stream()
                .collect(
                        // 分组收集
                        Collectors.groupingBy(sku -> sku.getSkuCategory())
                );

        System.out.println(JSON.toJSONString(map, true));
    }

    /** partitioningBy 分区收集 */
    @Test
    public void testPartition(){

        // Map<Boolean, List>, false在前，true在后
        Map<Boolean, List<Sku>> map = skus.stream()
                .collect(
                        // 分区，返回一个Map<Boolean, List>
                        Collectors.partitioningBy(sku -> sku.getTotalPrice() > 1000)
                );


        System.out.println(JSON.toJSONString(map, true));
    }

    /** counting 计算数量 */
    @Test
    public void testCounting(){

        Long collect1 = skus.stream().collect(
                Collectors.counting()
        );
        // 9
        System.out.println(collect1);

    }
}
