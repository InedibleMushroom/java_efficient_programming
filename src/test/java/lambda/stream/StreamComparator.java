package lambda.stream;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * <h1>Stream 比较器<h1>
 *
 * @author: InedibleMushroom
 * @create: 2019-10-31 16:58
 **/
public class StreamComparator {

    private List<String> list = Arrays.asList("hi", "hello", "world", "welcome", "stream");

    @Test
    public void test(){

        // 按字符串长度升序排序
        //Collections.sort(list, (i1, i2) -> i1.length() - i2.length());

        // 按字符串长度降序排序
        // 使用reversed()方法不能使用 item -> item.length() 因为编译器无法推断出类型
        // 可以显示声明类型 (String item) -> item.length()
        Collections.sort(list, Comparator.comparingInt(String::length).reversed());
        System.out.println(list);

        list.sort(Comparator.comparingInt(String::length).reversed());
        System.out.println(list);

        // 多重排序
        // String.CASE_INSENSITIVE_ORDER 不区分大小写排序
        list.sort(Comparator.comparingInt(String::length).thenComparing(String.CASE_INSENSITIVE_ORDER).reversed());
        System.out.println(list);

        // 最后一次thenComparing(Comparator.reverseOrder()))不会执行，因为第二步已经比较成功且没有相等的（返回值为0的）
        Collections.sort(list, Comparator.comparingInt(String::length).reversed()
                .thenComparing(Comparator.comparing(String::toLowerCase, Comparator.reverseOrder()))
                .thenComparing(Comparator.reverseOrder()));
        System.out.println(list);


    }
}
