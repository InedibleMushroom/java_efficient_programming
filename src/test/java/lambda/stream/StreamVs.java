package lambda.stream;

import com.alibaba.fastjson.JSON;
import lambda.cart.CartService;
import lambda.cart.Sku;
import lambda.cart.SkuCategoryEnum;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * <h1>使用stream流编程改进</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-21 22:29
 */
public class StreamVs {

    /**
     * 需求
     * 1. 查看所有商品
     * 2. 过滤出图书类商品
     * 3. 其余商品中挑选出两件最贵的
     * 4. 只要这两件的名称和总价
     */

    /** 使用原始方式 */
    @Test
    public void oldCartHandler(){
        List<Sku> skus = CartService.getCartSkuList();

        // 1
        for (Sku sku : skus){
            System.out.println(JSON.toJSONString(sku, true));
        }
        // 2
        List<Sku> notBookSkus = new ArrayList<>();
        for (Sku sku : skus){
            if (!SkuCategoryEnum.BOOKS.equals(sku.getSkuCategory())){
                notBookSkus.add(sku);
            }
        }
        // 3
        notBookSkus.sort(new Comparator<Sku>() {
            @Override
            public int compare(Sku o1, Sku o2) {
                if (o1.getTotalPrice() > o2.getTotalPrice()){
                    return -1;
                } else if (o1.getTotalPrice() < o2.getTotalPrice()){
                    return 1;
                } else {
                    return 0;
                }
            }
        });
        List<Sku> top2SkuList = new ArrayList<>();
        for (int i = 0 ; i < 2; i++){
            top2SkuList.add(notBookSkus.get(i));
        }
        // 4
        Double money = 0.0;
        for (Sku sku : top2SkuList){
            money += sku.getTotalPrice();
        }
        List<String> name = new ArrayList<>();
        for (Sku sku : top2SkuList){
            name.add(sku.getName());
        }

        System.out.println(JSON.toJSONString(name, true));
        System.out.println(money);
    }

    /** 使用stream流方式 */
    @Test
    public void newCartHandler(){

        AtomicReference<Double> money = new AtomicReference<>(Double.valueOf(0.0));

        List<String> names = CartService.getCartSkuList().stream()
                // 1
                .peek(sku -> System.out.println(JSON.toJSONString(sku,true)))
                // 2
                .filter(sku -> !SkuCategoryEnum.BOOKS.equals(sku.getSkuCategory()))
                // 3    默认排序规则为从小到大，可以反转
                .sorted(Comparator.comparing(Sku::getTotalPrice).reversed())
                // 4
                .limit(2)
                .peek(sku -> money.set(money.get()+sku.getTotalPrice()))
                .map(sku -> sku.getName())
                .collect(Collectors.toList());

        names.forEach(System.out::println);
        System.out.println(money.get());

    }
}
