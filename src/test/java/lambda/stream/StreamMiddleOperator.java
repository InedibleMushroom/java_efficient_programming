package lambda.stream;

import com.alibaba.fastjson.JSON;
import lambda.cart.CartService;
import lambda.cart.Sku;
import lambda.cart.SkuCategoryEnum;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * <h1>流操作演示</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-22 00:47
 */
public class StreamMiddleOperator {

    List<Sku> skus;

    @Before
    public void init(){
        skus = CartService.getCartSkuList();
    }

    /** filter操作 */
    @Test
    public void testFilter(){
        skus.stream()
                .filter(
                        // filter 过滤 调用 Predicate函数式接口 返回类型为 Boolean
                        // 将流中的元素过滤出符合条件的元素
                        sku -> SkuCategoryEnum.BOOKS.equals(sku.getSkuCategory())
                )
                .forEach(sku -> System.out.println(JSON.toJSONString(sku, true)));
    }

    /** map操作 */
    @Test
    public void testMap(){
        skus.stream()
                .map(
                        // map 映射 调用 Function接口 返回类型为另一种类型
                        // 将流中的元素类型转换为另一种类型
                        sku -> sku.getName()
                )
                .forEach(sku -> System.out.println(JSON.toJSONString(sku, true)));
    }

    /** flatMap操作 */
    @Test
    public void testFlatMap(){
        skus.stream()
                .flatMap(
                        // flatMap 扁平化操作 调用Function函数式接口 返回类型为流
                        // 将流中的元素转换为另一种流
                        sku -> Arrays.stream(sku.getName().split(""))
                )
                .forEach(sku -> System.out.println(JSON.toJSONString(sku, true)));
    }

    /** peek操作 */
    @Test
    public void testPeek(){
        skus.stream()
                .peek(
                        // peek 遍历元素 调用Consumer函数式接口 返回类型为void
                        // 对元素进行遍历
                        System.out::println
                )
                .forEach(sku -> System.out.println(JSON.toJSONString(sku,true)));
    }


    /** sort操作 */
    @Test
    public void testSort(){
        skus.stream()
                .peek(System.out::println)
                .sorted(
                        // sort 对元素排序 调用Consumer函数式接口 返回类型 void
                        // 对元素进行排序
                        Comparator.comparing(Sku::getSkuPrice).reversed()
                )
                .forEach(sku -> System.out.println(JSON.toJSONString(sku,true)));
    }


    /** distinct操作 */
    @Test
    public void testDistinct(){
        skus.stream()
                .map(sku -> sku.getSkuCategory())

                // distinct 去重 无参
                // 将重复的元素过滤出去
                .distinct()

                .forEach(System.out::println);
    }


    /** skip, limit操作 */
    @Test
    public void testSkip(){
        skus.stream()
                .sorted(Comparator.comparing(Sku::getTotalPrice))

                // skip 跳过前几条元素
                .skip(5)

                // limit 截取前几条元素
                .limit(3)

                .forEach(sku -> System.out.println(JSON.toJSONString(sku,true)));
    }
}
