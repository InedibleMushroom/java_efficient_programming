package lambda.stream;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * <h1>流的构建四种形式</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-22 02:04
 */
public class StreamConstructor {

    /** 由值构建流 */
    @Test
    public void testStreamFromValue(){
        Stream stream = Stream.of(1,2,3,4,5);
        stream.forEach(System.out::println);
    }

    /** 由数组构建流 */
    @Test
    public void testStreamFromArray(){
        int[] numbers = {1,2,3,4,5,6};
        IntStream stream = Arrays.stream(numbers);
        stream.forEach(System.out::println);
    }


    /** 通过文件构建流 */
    @Test
    public void testStreamFromFile() throws IOException {
        Stream<String> stream = Files.lines(Paths.get("/Users/wangteng/GitRepo/java_efficient_programming/EfficientPrograming/src/test/java/lambda/stream/StreamConstructor.java"));
        stream.forEach(System.out::println);
    }


    /** 通过函数构建流 */
    @Test
    public void testStreamFromFunction(){
        // 通过迭代的方式生成流
        Stream stream = Stream.iterate(0, n -> n+2);
        stream.forEach(System.out::println);

        // 根据生成器生成流，后一个流并不会基于前一个流
        Stream stream1 = Stream.generate(Math::random);
        stream1.forEach(System.out::println);
    }

}
