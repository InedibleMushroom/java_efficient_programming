package lambda.stream;

import com.alibaba.fastjson.JSON;
import lambda.cart.CartService;
import lambda.cart.Sku;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.OptionalInt;

/**
 * <h1>stream流终端操作</h1>
 *
 * @auther: InedibleMushroom
 * @create: 2019-09-22 01:33
 */
public class StreamEndOperator {

    List<Sku> skus;

    @Before
    public void init(){
        skus = CartService.getCartSkuList();
    }

    /** allMatch操作 */
    @Test
    public void testAllMatch(){
        boolean match = skus.stream()
                .allMatch(

                        // allMatch 所有匹配返回true，反之返回false，短路操作
                        sku -> sku.getTotalPrice() > 100
                );
        System.out.println(match);

    }

    /** anyMatch操作 */
    @Test
    public void testAnyMatch(){
        boolean match = skus.stream()
                .anyMatch(

                        // anyMatch 只要有一个所有匹配返回true，反之返回false
                        sku -> sku.getTotalPrice() > 100
                );
        System.out.println(match);

    }

    /** noneMatch操作 */
    @Test
    public void testNoneMatch(){
        boolean match = skus.stream()
                .noneMatch(

                        // noneMatch 所有都不匹配返回true，反之返回false
                        sku -> sku.getTotalPrice() > 10000
                );
        System.out.println(match);

    }

    /** findFirst操作 */
    @Test
    public void testFindFirst(){
        Optional<Sku> optionalSku = skus.stream()

                // 找到流中的第一个元素 无参
                .findFirst();

        System.out.println(JSON.toJSONString(optionalSku.get(), true));
    }

    /** findAny操作 */
    @Test
    public void testFindAny(){
        Optional<Sku> optionalSku = skus.stream()

                // 找到流中的任意一个元素 无参
                // 即只要有就返回
                // 它比findFirst的优点就是在并行处理方面速度快
                .findAny();

        System.out.println(JSON.toJSONString(optionalSku.get(), true));
    }


    /** max, min, count 操作 */
    @Test
    public void testMaxMinCount(){

        OptionalDouble max = skus.stream()
                .mapToDouble(Sku::getTotalPrice)
                // 寻找最大值
                .max();

        OptionalDouble min = skus.stream()
                .mapToDouble(Sku::getTotalPrice)
                // 寻找最小值
                .min();

        long count = skus.stream()
                // 计数
                .count();

        System.out.println(max.getAsDouble());
        System.out.println(min.getAsDouble());
        System.out.println(count);

    }
}
